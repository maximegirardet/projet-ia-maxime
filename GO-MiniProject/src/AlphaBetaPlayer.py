# -*- coding: utf-8 -*-
""" This is the file you have to modify for the tournament. Your default AI player must be called by this module, in the
myPlayer class.

Right now, this class contains the copy of the randomPlayer. But you have to change this!
"""

import Goban
from random import choice
from playerInterface import *

openingMoves = {
    "primaryMoves":
    [
        "G7",
        "C7",
        "C3",
        "G3"
    ],
    "secondaryMoves": [
        "H7"
        "G8",
        "B7",
        "C8",
        "C2",
        "B2",
        "H3",
        "H2"
    ]
}



# noinspection PyAttributeOutsideInit
class myPlayer(PlayerInterface):
    def __init__(self):
        self._board = Goban.Board()
        self._mycolor = None
        self.nbmoves = 0
        self.time = 0

    def getPlayerName(self):
        return "First Minmax Player"

    def getPlayerMove(self):
        self.nbmoves += 1
        if self._board.is_game_over():
            print("Referee told me to play but the game is over!")
            return "PASS"

        if self.nbmoves < 5:
            move = self.openingMove()
        else:
            move = self.MaxValueCoup()
        self._board.push(move)
        # New here: allows to consider internal representations of moves
        print("I am playing ", self._board.move_to_str(move))
        print("My current board :")
        self._board.prettyPrint()
        # move is an internal representation. To communicate with the interface I need to change if to a string
        return Goban.Board.flat_to_name(move)

    def playOpponentMove(self, move):
        print("Opponent played ", move)  # New here
        #  the board needs an internal represetation to push the move.  Not a string
        self._board.push(Goban.Board.name_to_flat(move))

    def newGame(self, color):
        self._mycolor = color
        self._opponent = Goban.Board.flip(color)

    def endGame(self, winner):
        if self._mycolor == winner:
            print("I won!!!")
        else:
            print("I lost :(!!")

    def randomMove(self):
        return choice([m for m in self._board.legal_moves()])

    def capture_diff(self):
        """Simple heuristic"""
        diff = Goban.Board.diff_stones_board(self._board)
        if self._mycolor == 1:
            return diff
        return -1 * diff  # Maybe a problem if we are not black

    def MaxValue(self, alpha, beta, heuristique, depth):
        if depth == 0 or self._board.is_game_over():
            return heuristique()
        for move in self._board.legal_moves():
            self._board.push(move)
            alpha = max(alpha, self.MinValue(alpha, beta, heuristique, depth - 1))
            self._board.pop()
            if alpha >= beta:
                return beta
        return alpha

    def MinValue(self, alpha, beta, heuristique, depth):
        if depth == 0 or self._board.is_game_over():
            return heuristique()
        for move in self._board.legal_moves():
            self._board.push(move)
            beta = min(beta, self.MaxValue(alpha, beta, heuristique, depth - 1))
            self._board.pop()
            if alpha >= beta:
                return alpha
        return beta

    def MaxValueCoup(self, depth=3):  # depth : iterative deepening
        alpha = -1000000
        beta = 1000000
        meilleurCoup = 0
        if depth == 0:
            return self.capture_diff()
        for move in self._board.legal_moves():
            alpha1 = alpha
            self._board.push(move)
            alpha = max(alpha, self.MinValue(alpha, beta, self.capture_diff, depth - 1))
            self._board.pop()
            if alpha >= beta:
                return move
            if alpha1 != alpha:
                meilleurCoup = move
        if meilleurCoup != 0:
            return meilleurCoup
        else:
            return self.randomMove()

    def openingMove(self):
        move = None
        for m in self._board.legal_moves():
            if self._board.move_to_str(m) in openingMoves["primaryMoves"]:
                return m
            elif self._board.move_to_str(m) in openingMoves["secondaryMoves"]:
                move = m
        if move is not None:
            return move
        return self.randomMove()
