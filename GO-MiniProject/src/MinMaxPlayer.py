# -*- coding: utf-8 -*-
""" This is the file you have to modify for the tournament. Your default AI player must be called by this module, in the
myPlayer class.

Right now, this class contains the copy of the randomPlayer. But you have to change this!
"""

import Goban
from random import choice
from playerInterface import *

MAX_SCORE = 999999
MIN_SCORE = -999999

MAX_DEPTH = 2


# noinspection PyAttributeOutsideInit
class myPlayer(PlayerInterface):
    """ Example of a random player for the go. The only tricky part is to be able to handle
    the internal representation of moves given by legal_moves() and used by push() and
    to translate them to the GO-move strings "A1", ..., "J8", "PASS". Easy!

    """

    def __init__(self):
        self._board = Goban.Board()
        self._mycolor = None

    def getPlayerName(self):
        return "First Minmax Player"

    def getPlayerMove(self):
        if self._board.is_game_over():
            print("Referee told me to play but the game is over!")
            return "PASS"
        moves = self._board.legal_moves()  # Dont use weak_legal_moves() here!
        move = self.select_move(moves, MAX_DEPTH, self.capture_diff)
        self._board.push(move)

        # New here: allows to consider internal representations of moves
        print("I am playing ", self._board.move_to_str(move))
        print("My current board :")
        self._board.prettyPrint()
        # move is an internal representation. To communicate with the interface I need to change if to a string
        return Goban.Board.flat_to_name(move)

    def playOpponentMove(self, move):
        print("Opponent played ", move)  # New here
        #  the board needs an internal represetation to push the move.  Not a string
        self._board.push(Goban.Board.name_to_flat(move))

    def newGame(self, color):
        self._mycolor = color
        self._opponent = Goban.Board.flip(color)

    def endGame(self, winner):
        if self._mycolor == winner:
            print("I won!!!")
        else:
            print("I lost :(!!")

    def capture_diff(self):
        """Simple heuristic"""
        diff = Goban.Board.diff_stones_board(self._board)
        if self._mycolor == 1:
            return diff
        return -1 * diff  # Maybe a problem if we are not black

    def minmax_result(self, max_depth, heuristic_fn):
        """minimax with pruning"""
        if self._board.is_game_over():
            if (Goban.Board.result(self._board) == "1-0" and self._mycolor == 2) or (
                    Goban.Board.result(self._board) == "0-1" and self._mycolor == 1):
                # game_state.winner() == game_state.next_player:
                return MAX_SCORE
            else:
                return MIN_SCORE

        if max_depth == 0:
            return heuristic_fn()

        best_so_far = MIN_SCORE
        for candidate_move in self._board.legal_moves():
            self._board.push(candidate_move)  # Maybe verify if the move can be played
            opponent_best_result = self.minmax_result(max_depth - 1, heuristic_fn)
            our_result = -1 * opponent_best_result
            if our_result > best_so_far:
                best_so_far = our_result
            self._board.pop()

        return best_so_far

    def select_move(self, moves, max_depth, heuristic_fn):
        best_moves = []
        best_score = None
        # Loop over all legal moves.
        for possible_move in moves:
            # Calculate the game state if we select this move.
            self._board.push(possible_move)
            # Since our opponent plays next, figure out their best
            # possible outcome from there.
            opponent_best_outcome = self.minmax_result(max_depth, heuristic_fn)
            # Our outcome is the opposite of our opponent's outcome.
            our_best_outcome = -1 * opponent_best_outcome
            if (not best_moves) or our_best_outcome > best_score:
                # This is the best move so far.
                best_moves = [possible_move]
                best_score = our_best_outcome
            elif our_best_outcome == best_score:
                # This is as good as our previous best move.
                best_moves.append(possible_move)
            # For variety, randomly select among all equally good moves.
            self._board.pop()
        return choice(best_moves)
